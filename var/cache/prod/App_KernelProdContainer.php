<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerUoMxVHA\App_KernelProdContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerUoMxVHA/App_KernelProdContainer.php') {
    touch(__DIR__.'/ContainerUoMxVHA.legacy');

    return;
}

if (!\class_exists(App_KernelProdContainer::class, false)) {
    \class_alias(\ContainerUoMxVHA\App_KernelProdContainer::class, App_KernelProdContainer::class, false);
}

return new \ContainerUoMxVHA\App_KernelProdContainer([
    'container.build_hash' => 'UoMxVHA',
    'container.build_id' => '4772b498',
    'container.build_time' => 1598082584,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerUoMxVHA');
