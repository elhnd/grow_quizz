<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\QuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=QuestionRepository::class)
 * @ApiResource(
 *  normalizationContext={
 *      "groups"={"question_read"}
 *  },
 * 
 * )
 * 
 */
class Question
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"question_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="text",nullable=true)
     * @Groups({"question_read"})
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=Response::class, mappedBy="question",cascade={"persist"})
     * @Groups({"question_read"})
     */
    private $responses;

    /**
     * @ORM\OneToMany(targetEntity=UserResponse::class, mappedBy="question")
     */
    private $userResponses;

    public function __construct()
    {
        $this->responses = new ArrayCollection();
        $this->userResponses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|Response[]
     */
    public function getResponses(): Collection
    {
        return $this->responses;
    }

    public function addResponse(Response $response): self
    {
        if (!$this->responses->contains($response)) {
            $this->responses[] = $response;
            $response->setQuestion($this);
        }

        return $this;
    }

    public function removeResponse(Response $response): self
    {
        if ($this->responses->contains($response)) {
            $this->responses->removeElement($response);
            // set the owning side to null (unless already changed)
            if ($response->getQuestion() === $this) {
                $response->setQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserResponse[]
     */
    public function getUserResponses(): Collection
    {
        return $this->userResponses;
    }

    public function addUserResponse(UserResponse $userResponse): self
    {
        if (!$this->userResponses->contains($userResponse)) {
            $this->userResponses[] = $userResponse;
            $userResponse->setQuestion($this);
        }

        return $this;
    }

    public function removeUserResponse(UserResponse $userResponse): self
    {
        if ($this->userResponses->contains($userResponse)) {
            $this->userResponses->removeElement($userResponse);
            // set the owning side to null (unless already changed)
            if ($userResponse->getQuestion() === $this) {
                $userResponse->setQuestion(null);
            }
        }

        return $this;
    }
}
