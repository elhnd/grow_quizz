<?php

namespace App\EventsSubscriber\Quizz;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Question;
use App\Entity\Response;
use App\Entity\UserResponse;
use App\Repository\QuestionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;


class Quizz extends AbstractController implements EventSubscriberInterface
{
    private $em;
    private $questionRepository;

    public function __construct(EntityManagerInterface $em, QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['addQuizz', EventPriorities::PRE_WRITE]
        ];
    }

    public function addQuizz(ViewEvent $event)
    {

        $instance = $event->getControllerResult();

        $method = $event->getRequest()->getMethod();
        $data = json_decode($event->getRequest()->getContent(), true);
        if ($instance instanceof Question && $method === "POST") {
            $question = new Question();
            $question->setLibelle($data['question']['libelle']);

            for ($i = 0; $i < count($data['response']); $i++) {
                $response = new Response();
                $response->setQuestion($question);
                $response->setLibelle($data['response'][$i]['libelle']);
                $response->setValide($data['response'][$i]['valide']);
                $this->em->persist($response);
            }
            $this->em->persist($question);
            $this->em->flush();
            die;
        }

        if($instance instanceof UserResponse && $method=="POST") {
            //dd($data["resultat"]);
            /* 
                user_id
                question_id
                correction
                reponse
            */
            $user = $this->getUser();
           

            for ($i = 0; $i < count($data["resultat"]); $i++) {
                $userResponse = new UserResponse();
                $userResponse->setUser($user);
                $question =$this->questionRepository->find($data["resultat"][$i]["question"]['id']);
                $userResponse->setQuestion($question);
                $userResponse->setReponse($data["resultat"][$i]["response"]['valide']);
                if($userResponse->getReponse()==true){
                    $userResponse->setCorrection(true);
                } else{
                    $userResponse->setCorrection(false);
                }
               
                $this->em->persist($userResponse);
            }
            
            $this->em->flush();
            die;
        }
    }
}
