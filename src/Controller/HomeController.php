<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     *@Route("/{vueRouting}", requirements={"vueRouting"="^(?!api|_(profiler|wdt)).*"}, name="index")
     */
    public function index()
    {
        return $this->render('base.html.twig', []);
    }


    // /**
    //  *@Route("/fournisseur/add/produit/{id}", name="fournprod")
    //  */
    // public function fournProd()
    // {
    //     return $this->render('produitfourn.html.twig', []);
    // }
}
