import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../pages/Login'
import MainSlot from '../components/layout/MainSlot'
import store from '../store'
import UserList from '../pages/user/List'
import QuizzPanel from '../pages/quizz/QuizzPanel'
import Quizz from '../pages/quizz/Quizz'
import Home from '../pages/Home'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [{
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/',
    component: MainSlot,
    children: [
      {
        path: '/home',
        name: 'Home',
        meta: {
          requiresAuth: true
        },
        component: Home
      },
      {
        path: '/user/list',
        name: 'UserList',
        meta: {
          requiresAuth: true
        },
        component: UserList
      },
      {
        path: '/quizz/panel',
        name: 'QuizzPanel',
        meta: {
          requiresAuth: true
        },
        component: QuizzPanel
      },
      {
        path: '/quizz',
        name: 'Quizz',
        meta: {
          requiresAuth: true
        },
        component: Quizz
      },
    ],
    redirect: { name: 'Home' },
    meta: {
      requiresAuth: true,
      bodyClass: 'hold-transition skin-blue sidebar-mini'
    }
  }
  ]
})

router.beforeEach((to, from, next) => {
  const token = store.getters['auth/jwtDecoded'] || null
  const authorized = token && token.exp > Date.now() / 1000
  if (authorized) {
    if (to.matched.some(record => !record.meta.requiresAuth)) {
      next({ name: 'Home' })
    }
  } else if (to.matched.some(record => record.meta.requiresAuth)) {
    next({ name: 'Login' })
  }

  next()
})


export default router
