
export default function() {
    return {
      item: {
        isActivated: true,
        name:'',
        phone:'',
        email:'',
        commission:'',
        roles:["ROLE_VISITEUR"]
      },
      items: [],
      error: null,
      errors: {},
      countUserData:{
        totalUser:0,
        totalUserActifs:0,
        totalUserInactifs:0
      }
    }
  }
  